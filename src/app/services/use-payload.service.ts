import { Injectable } from '@angular/core';
import { MissionService } from './endpoints/mission.service';

var url = 'http://147.83.159.160:4200/';
@Injectable()
export class UsePayload {
    constructor(private missionService: MissionService) { }
    
    keys = new Array();
    values = new Array();

    getPayload(payload, endpoint, method) {

        var payload = JSON.parse(payload);

        if (method == 'GET') {

            if (payload['endpoint'] == 'mission') {
                this.missionService.getMissions().subscribe((res)=>{
                    return res;
                })
            };

            url += payload['endpoint'];
            const id = payload['id'];

            return 'GET de ' + url;
        };

        if (method == 'PUT') {

            return 'PUT a ' + endpoint;
        };
        

        //for (let [key, value] of Object.entries(payload)) {
         //   console.log(`${key}: ${value}`);
          //  this.keys.push(key);
          //  this.values.push(value);
        //};

        if (this.keys[0] == 'endpoint') {

            url += this.values[0];
        }
    };
}

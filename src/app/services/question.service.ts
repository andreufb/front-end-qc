import { Injectable }       from '@angular/core';

import { DropdownQuestion } from '../components/dynamic-form/question-dropdown';
import { QuestionBase }     from '../components/dynamic-form/question-base';
import { TextboxQuestion }  from '../components/dynamic-form/question-textbox';
import { variable } from '@angular/compiler/src/output/output_ast';

@Injectable()
export class QuestionService {

  // TODO: get from a remote source of question metadata
  getData() {

    let questions: QuestionBase<any>[] = [

      new DropdownQuestion({
        key: 'endpoint',
        label: 'Type of data',
        options: [
          {key: 'physical-system ',  value: 'Physical system'},
          {key: 'person',  value: 'Person'},
          {key: 'project',   value: 'Project'},
          {key: 'mission', value: 'Mission'},
          {key: 'action',  value: 'Action'},
          {key: 'location',  value: 'Location'},
          {key: 'feature-of-interest',   value: 'Feature of interest'},
          {key: 'observable-property', value: 'Observable property'},
          {key: 'unit-of-measurement',   value: 'Unit of measurement'},
          {key: 'observation', value: 'Observation'}
        ],
        order: 1,
        method: 'GET'
      }),

      new TextboxQuestion({
        key: 'id',
        label: 'ID',
        order: 2
      }),
    ];

    return questions.sort((a, b) => a.order - b.order);
  };

  getPhysicalSystemQuestions(){
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'identifiers',
        label: 'Identifyers',
        required: true,
        order: 1,
        endpoint: 'physical-system',
        method: 'PUT'
      }),

      new TextboxQuestion({
        key: 'contacts',
        label: 'Contacts',
        required: true,
        order: 2
      }),

      new TextboxQuestion({
        key: 'observableProperties',
        label: 'Observable properties',
        required: true,
        order: 3
      }),

      new TextboxQuestion({
        key: 'attachedTo',
        label: 'Attached to:',
        required: true,
        order: 4
      })
    ];

    return questions.sort((a, b) => a.order - b.order);
  };

  getPersonQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'name',
        label: 'Name',
        required: true,
        order: 1,
        endpoint: 'person',
        method: 'PUT'
      }),

      new TextboxQuestion({
        key: 'mail',
        label: 'Mail',
        required: true,
        order: 1
      }),

      new TextboxQuestion({
        key: 'role',
        label: 'Role',
        required: true,
        order: 1
      })
    ];

    return questions.sort((a, b) => a.order - b.order);
  };

  getMissionQuestions() {
    let questions: QuestionBase<any>[] = [

      new TextboxQuestion({
        key: 'label',
        label: 'Label',
        required: true,
        order: 1,
        endpoint: 'mission',
        method: 'PUT'
      }),

      new TextboxQuestion({
        key: 'description',
        label: 'Description',
        required: true,
        order: 2
      })
    ];

    return questions.sort((a, b) => a.order - b.order);
  };


}

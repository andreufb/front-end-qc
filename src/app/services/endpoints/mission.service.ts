import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';




class Mission {
  idmission: number;
  label: string;
  description: string;
}

class PutMission {
  label: string;
  description: string;
}

//public getMission(idmission: number) {}     // GET and POST
//public getMissions(url?: string) {}         // GET all
//public putMission(mission: PutMission) {}   // PUT
//public deleteMission(idmission: number) {}  // DELETE



@Injectable({
  providedIn: 'root'
})
export class MissionService {

  url: string = 'http://147.83.159.160:4200/mission';
  public firstPage: string = "";
  public prevPage: string = "";
  public nextPage: string = "";
  public lastPage: string = "";

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders ({
      'Content-Type': 'application/json'
    })
  }

  // GET with id
  getMission(idmission: number){
    return this.http.get<Mission>(this.url + '/' + idmission).pipe(
      retry(1),
      catchError(this.errorHandl)
    );
  };

  // GET all
  getMissions(): Observable<Mission[]>{
    return this.http.get<Mission[]>(this.url).pipe(
      retry(1),
      catchError(this.errorHandl)
    );
  };

  // POST
  // postMission(mission: Mission){
  //   return this.http.post(this.url, JSON.stringify(data), this.httpOptions)
  //   .pipe(
  //     retry(1),
  //     catchError(this.errorHandl)
  //   )
  // }

  // PUT
  putMission(mission: PutMission) {
    return this.http.put(this.url, mission)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    );
  };

  // DELETE
  deleteMission(idmission: number){
    return this.http.delete(this.url + '/' + idmission)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    );
  }

  // Error Handling
  errorHandl(error) {
    let errorMessage = 'Algo falla!';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }
}

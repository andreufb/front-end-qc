import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//Material
import { MatSliderModule } from '@angular/material/slider';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatCardModule} from '@angular/material/card';
import { MatIconModule} from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSnackBarModule }  from '@angular/material/snack-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule }  from '@angular/material/dialog';
import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ToolBarComponent } from './components/tool-bar/tool-bar.component';
import { LoginService } from './components/login/login.service';
import { UsePayload } from './services/use-payload.service';
import { MissionService } from './services/endpoints/mission.service';
import { DynamicFormComponent } from './components/dynamic-form/dynamic-form.component';
import { DynamicFormQuestionComponent } from './components/dynamic-form/dynamic-form-question.component';
import { DynamicFormDialogComponent } from './components/dynamic-form/dynamic-form-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ToolBarComponent,
    DynamicFormComponent,
    DynamicFormQuestionComponent,
    DynamicFormDialogComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    MatSliderModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTabsModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatSnackBarModule,
    MatRadioModule,
    MatDialogModule

  ],
  providers: [
    LoginService,
    UsePayload, 
    MissionService
  
  ],
  bootstrap: [AppComponent],
  entryComponents: [DynamicFormDialogComponent]
})
export class AppModule {
  constructor(){
  }
 }

import { Component } from '@angular/core';
import { QuestionService } from './services/question.service';
import { DynamicFormDialogComponent } from './components/dynamic-form/dynamic-form-dialog.component';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [QuestionService]
})

export class AppComponent {
  title = 'Front end QC';
  getData: any[];
  putPhysicalSystem: any[];
  putPerson: any[];
  putMission: any[];
  
  

  constructor(service: QuestionService, public dialog:
    MatDialog) {
    this.getData = service.getData();
    this.putPhysicalSystem = service.getPhysicalSystemQuestions();
    this.putPerson = service.getPersonQuestions();
    this.putMission = service.getMissionQuestions();
  }

 // For opening the questionary in a dialog //
  //openDialog() {
  //  const dialogRef = this.dialog.open(DynamicFormDialogComponent, {
  //    width: '450px',
  //    data: {examplequestions: this.examplequestions}
  //  });

  //  dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
  //  });
  //}

}
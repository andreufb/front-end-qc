import { LoginService } from './login.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user_email;
  user_password;

  message;

  user_input;

  token;


  takeInfo(evt: MouseEvent) {
    this.user_input = [this.user_email, this.user_password];
    this.message = "Token: " + this.user_input;

  }

  constructor(service: LoginService) {
  }

  ngOnInit(): void {
  }

}
